# GeoIP

This package imports geolocation information from (http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip)
and populates a SQLite3 database with that information.

It also exposes a method in `api/locationByIP?IP=[ip]` that returns that information.

It's using the [dingo/api](https://github.com/dingo/api) package for content negotation and api
related stuff.

## Decisions

I'm using the SQLite3 database for ease of use and rapid development. The solution could be adapted
or support any other database.

In addition, I've decided to use the native SQLite3 `.import` method import the CSV file, because it
is much faster. We could use an adapter for each database driver or make the import database independent,
using the Laravel database library, but that would be much slower.

## Creating the database

After creating the database file with `touch database/database.sqlite`,
simply run the migrations (`artisan migrate`).

The migration can be consulted at `database/migrations/2018_02_25_153437_geoip.php`.

## The importing process

The importing process is callable with an artisan command (`artisan geoip:populate`). It checks if the table is empty or
not. It also accepts a `--force` option to force the execution, dropping the existing data.

## The API

### Fetch country by IP [GET /api/locationByIP]
Fetches a country by IP

+ Request (application/json)
    + Body

            {
                "IP": "127.0.0.1"
            }

+ Response 200 (application/json)
    + Body

            {
                "country": "Norway",
                "country_alpha_2": "NO"
            }

## Installation process

1. Clone the project
2. Install the dependencies `composer install`
3. Create the database: `touch database/database.sqlite`
4. Run the migrations: `artisan migrate`
5. Populate the database: `artisan geoip:populate`
6. Run the Laravel locally: `artisan serve`
7. In your browser, access: `http://127.0.0.1:8000/api/locationByIP?IP=2.148.0.0`

## Testing

A simple end to end test was added that assumes the application is fully provisioned
(database populated, Laravel is running...).

Run it with `./vendor/bin/phpunit`.

## Requirements

* The usual Laravel requirements
* Both sqlite3 and php7.1-sqlite3 (could be adapted to any other database)
 