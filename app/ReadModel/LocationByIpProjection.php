<?php

declare(strict_types=1);

namespace App\ReadModel;


use Illuminate\Database\Connection;

class LocationByIpProjection
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function execute(string $ip)
    {
        $longIp = ip2long($ip);
        $rs = $this->connection->select(<<<'SQL'
SELECT country_alpha_2, country
FROM geoip
WHERE
  ? BETWEEN ip_long_start AND ip_long_end
LIMIT 1
SQL
            , [$longIp]);

        if (!empty($rs)) {
            return (array)$rs[0];
        }

        return [];
    }
}