<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Connection;

class PopulateGeoip extends Command
{
    private const GEOIP_ZIP_RESOURCE = 'http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip';
    private const GEOIP_FILE_NAME = 'GeoIPCountryWhois.csv';
    private const IMPORT_CSV_SQLITE = <<<'SQL'
BEGIN;
DELETE FROM geoip;
.mode csv
.import {{csv}} geoip
COMMIT;
SQL;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geoip:populate
     {source='.self::GEOIP_ZIP_RESOURCE.' : GeoIP information source}
     {--force : Wether to force the execution in case the information exists}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates the geoip table';
    /**
     * @var Connection
     */
    private $conn;

    /**
     * Create a new command instance.
     *
     * @param Connection $conn
     */
    public function __construct(Connection $conn)
    {
        parent::__construct();
        $this->conn = $conn;
    }

    /**
     * Execute the console command.
     * @throws \Throwable
     */
    public function handle()
    {
        if (!$this->option('force') && !$this->empty()) {
            $this->output->writeln('Database already populated. Use --force option to execute anyway.');
            return;
        }
        $csvFilePath = $this->fetchCSV();
        $this->loadToDatabase($csvFilePath);
        $this->output->success('Imported successfully!');
    }

    /**
     * @param $csvFile
     * @throws \Throwable
     */
    private function loadToDatabase($csvFile)
    {
        $this->output->writeln('Loading into database...');
        $sql = strtr(self::IMPORT_CSV_SQLITE, ['{{csv}}' => $csvFile]);
        exec("echo '$sql' | sqlite3 ".$this->conn->getDatabaseName());
    }

    private function fetchCSV()
    {
        $this->output->writeln('Downloading contents...');
        $tmpFile = tempnam(sys_get_temp_dir(), 'geoip');
        $rs = file_put_contents($tmpFile, fopen($this->argument('source'), 'r'));
        if (!$rs) {
            throw new \RuntimeException('Couldn\'t read geoip file!');
        }

        $this->output->writeln('Extracting...');
        $zip = new \ZipArchive();
        $resource = $zip->open($tmpFile);
        if ($resource !== true) {
            throw new \RuntimeException("Couldn't open zip file! Error number: $resource");
        }

        $directory = pathinfo($tmpFile, PATHINFO_DIRNAME);
        $zip->extractTo($directory);
        $file = "$directory/".self::GEOIP_FILE_NAME;
        if (!file_exists($file)) {
            throw new \RuntimeException('Expected file "'.self::GEOIP_FILE_NAME.'" doesn\'t exist!');
        }
        $this->output->writeln("Extracted to $file");
        return $file;
    }

    /**
     * Find out if the table is empty
     * @return bool
     */
    private function empty()
    {
        return !$this->conn->select('select exists(select null from geoip) ex')[0]->ex;
    }
}
