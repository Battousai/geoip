<?php

namespace App\Http\Controllers;


use App\Http\Requests\LocationByIpRequest;
use App\ReadModel\LocationByIpProjection;
use Dingo\Api\Routing\Helpers;

class LocationByGeoIp
{
    use Helpers;

    /**
     * @var LocationByIpProjection
     */
    private $projection;

    public function __construct(LocationByIpProjection $projection)
    {
        $this->projection = $projection;
    }

    /**
     * Fetch country by IP
     *
     * Fetches a country by IP
     *
     * @Get("api/locationByIP")
     * @Versions({"v1"})
     * @Transaction({
     * @Request({"IP"="127.0.0.1"}, contentType="application/json"),
     * @Response(200, body={"country":"Norway","country_alpha_2":"NO"})
     * })
     * @param LocationByIpRequest $request
     * @return array|void
     */
    public function __invoke(LocationByIpRequest $request)
    {
        $result = $this->projection->execute($request->get('IP'));
        if (empty($result)) {
            return $this->response->error('Resource not found', 404);
        }
        return $result;
    }
}