<?php

namespace Tests\Feature;


use Tests\TestCase;

class LocationByIPTest extends TestCase
{
    public function testNorway()
    {
        $response = $this->get('api/locationByIP?IP=2.148.0.0');
        $response->assertStatus(200);
        $obj = json_decode($response->getContent());
        $this->assertEquals('Norway', $obj->country);
        $this->assertEquals('NO', $obj->country_alpha_2);
    }

    public function testNotFound()
    {
        $response = $this->get('api/locationByIP?IP=0.0.0.0');
        $response->assertStatus(404);
    }
}