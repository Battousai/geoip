<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Geoip extends Migration
{
    private const TABLE = 'geoip';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE, function (Blueprint $table) {
            $table->string('ip_start');
            $table->string('ip_end');
            $table->unsignedInteger('ip_long_start');
            $table->unsignedInteger('ip_long_end');
            $table->char('country_alpha_2', 2);
            $table->string('country');
            $table->primary(['ip_long_start', 'ip_long_end']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE);
    }
}
